"""
Module reading adn writing csv files...

Module have 2 function:
    - dump_to_csv(filepath, lst) -> None
    - load_from_csv(filepath) -> List of params
"""
import os
import csv


def dump_to_csv(filepath, lst):
    """
    Save list of params to csv file...

    :type filepath: str
    :param filepath: Path to save .csv file
    :type lst: list of dict
    :param lst: [{'param_name': prm_value, ...}, ...]
    :return: None
    """
    if not lst:
        raise ValueError('List of params is empty')

    if os.path.exists(filepath):
        raise ValueError('File `{}` exists'.format(filepath))

    # Write Headers to Csv
    headers = [key for key in sorted(lst[0])]
    with open(filepath, 'w') as csv_file:
        csv_file.write('sep=,\n')
        csv_writer = csv.writer(
            csv_file,
            delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_NONNUMERIC
        )
        csv_writer.writerow(headers)

        # Write Values to Csv
        for params in lst:
            values = [params[key] for key in sorted(params)]
            csv_writer.writerow(values)


def load_from_csv(filepath):
    """
    Load list of params from csv file...

    :type filepath: str
    :param filepath: Path to load .csv file
    :return: list of dict [{'param_name': prm_value, ...}, ...]
    """
    if not os.path.exists(filepath):
        raise ValueError('File `{}` NOT exists'.format(filepath))

    lst = []
    with open(filepath, 'r') as csv_file:
        csv_reader = csv.reader(
            csv_file
        )

        headers = []
        for row in csv_reader:
            if row and row[0].startswith('sep='):
                continue

            # Save headers
            if not headers:
                headers = row

            # Save values
            else:
                params = {h: v for h, v in zip(headers, row)}
                lst.append(params)
    return lst


if __name__ == '__main__':
    lst = [
        {'id_tmo': 1, 'tmo_name': 'MY_TMO', 'mo_ref': 'BSC=1;BTS=1;MY_TMO=1'},
        {'id_tmo': 2, 'tmo_name': 'MY_TMO', 'mo_ref': 'BSC=1,BTS=1,MY_TMO=2'},
        {'id_tmo': 3, 'tmo_name': 'MY_TMO', 'mo_ref': 'BSC=1/BTS=1/MY_TMO=3'},
    ]

    dump_to_csv('/Users/shotinleg/test_csv.csv', lst)
    lst = load_from_csv('/Users/shotinleg/test_csv.csv')

    print('[')
    for x in lst:
        print('    {},'.format(x))
    print(']')
