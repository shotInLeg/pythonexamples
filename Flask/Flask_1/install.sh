#!/bin/bash

# Install virtaulenv
sudo apt-get install python-virtualenv -y

# Make Virtual Environment
virtualenv ~/venv/flask

# Make dir venv
mkdir -p ~/venv

# Make it active
source ~/venv/flask/bin/activate

# Install Flask
pip install flask

# Install requests
pip install requests

# Deactivate
deactivate

# Show message
echo "Now you can run ./run.sh"
