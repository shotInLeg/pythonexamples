#!/bin/bash

# Activate Virtual Environment
source ~/venv/flask/bin/activate

# Show message
echo "After you see 'Running on http://[::]:8080/' open browser at url http://localhost:8080"

# Run project
python src/main.py 8080

# Deactivate
deactivate
