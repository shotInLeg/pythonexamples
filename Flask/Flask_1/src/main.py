# coding: utf8
import sys

from web import app


def main():
    if len(sys.argv) != 2:
        print('python main.py <port>')
        sys.exit(0)

    app.web.run(host='::', port=int(sys.argv[1]), use_reloader=False, debug=True)


if __name__ == '__main__':
    main()
