# coding: utf8
import app
import flask


@app.web.route('/')
def root():
    return flask.render_template('index.html')


@app.web.route('/show/<name>')
def name(name):
    return flask.render_template('name.html', name=name)


@app.web.route('/show/<fname>/<lname>')
def full_name(fname, lname):
    return flask.render_template('full_name.html', fname=fname, lname=lname)
