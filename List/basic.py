lst1 = [1, "2", [1, "5"]]

lst1_copy = lst1[:] # Copy value from lst
lst1_copy2 = list(lst1) # Copy value from lst 
lst1_ref_copy = lst1 # Copy link to value from lst

lst1 = [x for x in range(1, 9)] # List generate
# [1, 2, 3, 4, 5, 6, 7, 8]

pair = [0, 1]
zero, one = pair

lst1.append(10) # Add element
# [1, 2, 3, 4, 5, 6, 7, 8, 10]

lst1.extend([11, 12]) # Add elements from list
# [1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12]

lst1.index(5) # Found index of value
lst1.count(5) # Count value in list

lst1.remove(5) # Delete element
# [1, 2, 3, 4, 6, 7, 8, 10, 11, 12]

lst1.sort() # Sort
lst1.reverse() # Reverse

lst1.pop() #
# [12, 11, 10, 8, 7, 6, 4, 3, 2]
 
len(lst1) # Count elements in list
max(lst1) # Max element in list
min(lst1) # Min element in list
