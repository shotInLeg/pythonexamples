# Install

```bash
./install.sh
```

# Run

```bash
./run.sh
```

# Deinstall

```bash
./deinstall.sh
```
