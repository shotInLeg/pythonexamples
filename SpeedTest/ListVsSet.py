import time

st = set()
lst2 = list()

speed = time.time()

print("Set: ")

start = time.time()
for x in xrange(0, 600000000):
	st.add(x)
print("    Add: {}".format(time.time() - start))

start = time.time()
if 500 in st:
	print("    Find 500: {}".format(time.time() - start))

start = time.time()
if 5000 in st:
	print("    Find 5000: {}".format(time.time() - start))

start = time.time()
if 50000 in st:
	print("    Find 50000: {}".format(time.time() - start))

start = time.time()
if 500000 in st:
	print("    Find 500000: {}".format(time.time() - start))

start = time.time()
if 5000000 in st:
	print("    Find 5000000: {}".format(time.time() - start))

start = time.time()
if 50000000 in st:
	print("    Find 50000000: {}".format(time.time() - start))

start = time.time()
if 99999999 in st:
	print("    Find 50000000: {}".format(time.time() - start))

print(time.time() - speed)
print(st.__sizeof__() / 1024 / 1024)


speed = time.time()

print("List: ")

start = time.time()
for x in xrange(0, 600000000):
	lst2.append(x)
print("    Add: {}".format(time.time() - start))

start = time.time()
if 500 in lst2:
	print("    Find 500: {}".format(time.time() - start))

start = time.time()
if 5000 in lst2:
	print("    Find 5000: {}".format(time.time() - start))

start = time.time()
if 50000 in lst2:
	print("    Find 50000: {}".format(time.time() - start))

start = time.time()
if 500000 in lst2:
	print("    Find 500000: {}".format(time.time() - start))

start = time.time()
if 5000000 in lst2:
	print("    Find 5000000: {}".format(time.time() - start))

start = time.time()
if 50000000 in lst2:
	print("    Find 50000000: {}".format(time.time() - start))

start = time.time()
if 99999999 in lst2:
	print("    Find 50000000: {}".format(time.time() - start))

print(time.time() - speed)
print(lst2.__sizeof__() / 1024 / 1024)


